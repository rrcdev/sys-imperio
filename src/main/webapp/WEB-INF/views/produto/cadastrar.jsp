<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:if test="${action eq 'cadastrar' }">
	<c:url var="url" value="/produto/cadastrar"></c:url>
	<c:set var="titulo" value="Novo Produto"></c:set>
	<c:set var="botao" value="Cadastrar"></c:set>
</c:if>
<c:if test="${action eq 'editar' }">
	<c:url var="url" value="/produto/editar/${produto.id }"></c:url>
	<c:set var="titulo" value="Editar Produto"></c:set>
	<c:set var="botao" value="Atualizar"></c:set>
</c:if>

<html>
<head>
	<jsp:include page="../header.jsp" />
	<title>${titulo}</title>
</head>
<body>
	
	<jsp:include page="../header-imperio.jsp" />
	
	<div class="container">
	<div>
		<h3 class="section">${titulo }</h3>
	</div>
		
		<form:form id="cadastrarProduto" commandName="produto"
			servletRelativeAction="${url }" method="POST">
			<fieldset>
				<div class="form-group">
					<label for="codProduto" class="col-lg-2 control-label">Código
						do Produto</label>
					<div class="col-lg-10">
						<form:input class="form-control valid-cod" id="codProduto"
							placeholder="Código do produto" type="text" path="codProduto" />
						<div class="error-validation">
							<form:errors path="codProduto"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<label for="nome" class="col-lg-2 control-label">Nome do
						Produto</label>
					<div class="col-lg-10">
						<form:input class="form-control" id="nome"
							placeholder="Nome do produto" type="text" path="nome" />
						<div class="error-validation">
							<form:errors path="nome"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<label for="qtdEstoque" class="col-lg-2 control-label">Qtd. em Estoque</label>
					<div class="col-lg-10">
						<form:input class="form-control only-num" id="qtdEstoque"
							placeholder="Quantidade em estoque" path="qtdEstoque" />
						<div class="error-validation">
							<form:errors path="qtdEstoque"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<label for="valorUnitEntrada" class="col-lg-2 control-label">Valor de Entrada</label>
					<div class="col-lg-10">
						<form:input class="form-control valid-num" id="valorUnitEntrada"
							placeholder="Valor unitário de entrada"
							path="valorUnitEntrada" />
						<div class="error-validation">
							<form:errors path="valorUnitEntrada"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<label for="valorUnitSaida" class="col-lg-2 control-label">Valor de Saída</label>
					<div class="col-lg-10">
						<form:input class="form-control valid-num" id="valorUnitSaida"
							placeholder="Valor unitário de saída" 
							path="valorUnitSaida" />
						<div class="error-validation">
							<form:errors path="valorUnitSaida"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<label for="descricao" class="col-lg-2 control-label">Descrião
						do Produto</label>
					<div class="col-lg-10">
						<form:textarea class="form-control" rows="3" id="descricao"
							path="descricao" />
						<span class="help-block">Escreva a descrição do produto a
							ser cadastrado.</span>
						<div class="error-validation">
							<form:errors path="descricao"></form:errors>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<a href="#" class="btn btn-default back">Cancelar</a>
						<button type="submit" class="btn btn-success">${botao }</button>
					</div>
				</div>
			</fieldset>
		</form:form>
	</div>

	<jsp:include page="../footer.jsp" />
</body>
</html>
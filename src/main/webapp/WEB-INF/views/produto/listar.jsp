<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<jsp:include page="../header.jsp" />
<title>Listar Produtos</title>
</head>
<body>

	<jsp:include page="../header-imperio.jsp" />

	<div class="container">

		<h3 class="section">Buscar Produto</h3>

		<form:form id="formBuscarProduto" role="form"
			servletRelativeAction="/produto/buscar" method="POST"
			class="bs-component">

			<div class="form-group">
				<div class="input-group">
					<input id="codProduto" name="codProduto" type="text"
						class="form-control valid-cod" placeholder="Código do produto..."
						size="40" required="required" value="${codProduto }"
						autofocus="autofocus" /> <span class="input-group-btn">
						<button class="btn btn-default" name="submit" type="submit">
							<span class="glyphicon glyphicon-search"></span> Buscar
						</button>
					</span>
				</div>
			</div>
		</form:form>

		<c:if test="${not empty erro}">
			<div class="alert alert-danger alert-dismissible" role="alert"
				id="alert-erro">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${erro}"></c:out>
			</div>
		</c:if>
		<c:if test="${not empty info}">
			<div class="alert alert-success alert-dismissible" role="alert"
				id="alert-info">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${info}"></c:out>
			</div>
		</c:if>

		<br> <br>

		<h3 class="section">
			<span class="glyphicon glyphicon-tags"></span> Produtos
		</h3>

		<div class="row">
			<div class="col-sm-12" align="right">
				<a href="<c:url value="/venda/buscar" />" class="btn btn-default"><span
					class="glyphicon glyphicon-shopping-cart"></span> Nova Venda</a> <a
					class="btn btn-success" href="<c:url value="/produto/cadastrar" />">Cadastrar
					Produto</a>
			</div>
		</div>
		<br> <br>

		<c:if test="${empty produtos }">
			<div class="alert alert-warning" role="alert">Nenhum produto
				cadastrado.</div>
		</c:if>

		<c:if test="${not empty produtos }">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Código do Prduto</th>
						<th>Nome</th>
						<th>Estoque</th>
						<th>Editar</th>
						<th>Remover</th>
						<th>Detalhes</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${produtos}" var="produto" varStatus="cont">
						<tr class="active">
							<td>${cont.count }</td>
							<td>${produto.codProduto }</td>
							<td>${produto.nome }</td>
							<td>${produto.qtdEstoque }</td>
							<td><a class="btn btn-info"
								href="<c:url value="/produto/editar/${produto.id }"></c:url>">
									<span class="glyphicon glyphicon-pencil"></span>
							</a></td>
							<td><a class="btn btn-primary"
								data-href="<c:url value="/produto/remover/${produto.id }"></c:url>"
								data-toggle="modal" data-target="#confirm-delete"> <span
									class="glyphicon glyphicon-trash"></span>
							</a></td>
							<td><a class="btn btn-warning"
								href="<c:url value="/produto/detalhe/${produto.id }"></c:url>">
									<span class="glyphicon glyphicon-eye-open"></span>
							</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

		<div id="confirm-delete" class="modal" role="dialog" tabindex="-1"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Confirmar Deleção</h4>
					</div>
					<div class="modal-body">
						<p>Você está a ponto de excluir um produto do estoque, esse
							procedimento é irreversível.</p>
						<p>Você deseja continuar?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-primary btn-ok">Deletar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<jsp:include page="../footer.jsp" />
</body>
</html>
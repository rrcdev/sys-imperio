<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
	<jsp:include page="../header.jsp" />
	<title>Detalhes do Produto</title>
</head>
<body>

	<jsp:include page="../header-imperio.jsp" />
	
	<div class="container">
		<div>
			<h3 class="section">Detalhe do Produto </h3>
		</div>

		<div class="row">
			<div class="col-sm-12" align="right">
				<a href="#" class="btn btn-default back"><span
					class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
			</div>
		</div>
		<br> <br>
		<table class="table table-striped table-hover">
			<tbody>
				<tr>
					<td>Código do Produto:</td>
					<td>${produtoDetalhe.codProduto}</td>
				</tr>
				<tr>
					<td>Nome do Produto:</td>
					<td>${produtoDetalhe.nome}</td>
				</tr>
				<tr>
					<td>Quantidade em Estoque:</td>
					<td>${produtoDetalhe.qtdEstoque}</td>
				</tr>
				<tr>
					<td>Valor Unitário de Entrada:</td>
					<td>R$ ${produtoDetalhe.valorUnitEntrada},00</td>
				</tr>
				<tr>
					<td>Valor Unitário de Saída:</td>
					<td>R$ ${produtoDetalhe.valorUnitSaida},00</td>
				</tr>
				<tr>
					<td>Descrição do Produto:</td>
					<td>${produtoDetalhe.descricao}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<jsp:include page="../footer.jsp" />
</body>
</html>
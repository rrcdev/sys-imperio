<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
	<jsp:include page="../header.jsp" />
	<title>Listar Vendas</title>
</head>
<body>

	<jsp:include page="../header-imperio.jsp" />

	<div class="container">
	
	
		<h3 class="section">
			<span class="glyphicon glyphicon-usd"></span> Vendas
		</h3>
		
		<c:if test="${not empty erro}">
			<div class="alert alert-danger alert-dismissible" role="alert" id="alert-erro">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${erro}"></c:out>
			</div>
		</c:if>
		<c:if test="${not empty info}">
			<div class="alert alert-success alert-dismissible" role="alert"
				id="alert-info">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${info}"></c:out>
			</div>
		</c:if>
		
		<div class="row">
			<div class="col-sm-12" align="right">
				<a class="btn btn-success" href="<c:url value="/venda/buscar" />">Realizar Nova Venda </a>
			</div>
		</div>
		<br> <br>
		
		<c:if test="${empty vendas }"><div class="alert alert-warning" role="alert">Nenhuma venda cadastrada.</div></c:if>
		
		<c:if test="${not empty vendas }">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Código da Venda</th>
						<th>Data</th>
						<th>Valor Total</th>
						<th>Detalhes</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${vendas}" var="venda" varStatus="cont">
						<tr class="active">
							<td>${cont.count }</td>
							<td>${venda.codigoVenda }</td>
							<td>${venda.dataVenda }</td>
							<td>R$ ${venda.valorTotal }</td>
							<td><a class="btn btn-primary" href="<c:url value="/venda/detalhes/${venda.id} "/>"> <span
									class="glyphicon glyphicon-eye-open back"></span>
							</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
	
	<jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>
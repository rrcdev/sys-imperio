<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
	<jsp:include page="../header.jsp" />
	<title>Detalhes da Venda</title>
</head>
<body>

	<jsp:include page="../header-imperio.jsp" />
	
	<div class="container">
		<div>
			<h3 class="section">Detalhes da Venda </h3>
		</div>

		<div class="row">
			<div class="col-sm-12" align="right">
				<a href="#" class="btn btn-default back"><span
					class="glyphicon glyphicon-chevron-left"></span> Voltar</a>
			</div>
		</div>
		<br> <br>
		<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Código do Produto</th>
						<th>Nome do Produto</th>
						<th>Valor</th>
						<th>Quantidade</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${venda.produtosVenda}" var="venda" varStatus="cont">
						<tr class="active">
							<td>${cont.count }</td>
							<td>${venda.pk.produto.codProduto }</td>
							<td>${venda.pk.produto.nome }</td>
							<td>${venda.pk.produto.valorUnitSaida }</td>
							<td>${venda.quantidade}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
	</div>
	
	<jsp:include page="../footer.jsp" />
</body>
</html>
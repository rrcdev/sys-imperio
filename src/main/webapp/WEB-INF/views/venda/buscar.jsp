<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
	<title>Realizar Venda</title>
	<jsp:include page="../header.jsp" />
</head>
<body>

	<jsp:include page="../header-imperio.jsp" />

	<div class="container">

		<h3 class="section">Buscar Produto</h3>

		<form:form id="formBuscarProduto" role="form"
			servletRelativeAction="/venda/buscar" method="POST"
			class="bs-component">

			<div class="form-group">
				<div class="input-group">
					<input id="codProduto" name="codProduto" type="text"
						class="form-control valid-cod" placeholder="Código do produto..."
						size="40" required="required" value="${codProduto }"
						autofocus="autofocus" /> <span class="input-group-btn">
						<button class="btn btn-default" name="submit" type="submit">
							<span class="glyphicon glyphicon-search"></span> Buscar
						</button>
					</span>
				</div>
			</div>
		</form:form>

		<c:if test="${not empty erro}">
			<div class="alert alert-danger alert-dismissible" role="alert" id="alert-erro">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${erro}"></c:out>
			</div>
		</c:if>
		<c:if test="${not empty info}">
			<div class="alert alert-success alert-dismissible" role="alert" id="alert-info">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<c:out value="${info}"></c:out>
			</div>
		</c:if>

		<c:if test="${not empty codProduto and empty produtos }">
			<div class="alert alert-warning" role="alert">Nenhum produto encontrado.</div>
		</c:if>

		<c:if test="${not empty produtos }">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>Código do Produto</th>
						<th>Nome</th>
						<th>Valor R$</th>
						<th>Estoque</th>
						<th>Quantidade / Adicionar ao Carrinho</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${produtos }" var="produto">
						<tr class="active">
							<td>${produto.codProduto }</td>
							<td>${produto.nome }</td>
							<td>R$ ${produto.valorUnitSaida }</td>
							<td>${produto.qtdEstoque }</td>
							<td class="col-sm-3">
								<form:form id="adicionarProdutoCarrinho" role="form" servletRelativeAction="/venda/adicionar-no-carrinho/${produto.id}" method="POST" class="bs-component">
									<div class="form-group">
										<div class="input-group">
											<input id="quantidade" name="quantidade" 
												class="form-control only-num"
												placeholder="1" size="3"
												required="required" value="${quantidade }"
												autofocus="autofocus" /> <span class="input-group-btn">
												<button class="btn btn-success" name="submit" type="submit">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</span>
										</div>
									</div>
								</form:form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

		<br>
		<br>

		<h3 class="section">
			<span class="glyphicon glyphicon-shopping-cart"></span> Carrinho: <span class="corner">${valorTotal} </span> <span class="corner">R$ </span>
		</h3>
		
		<form:form id="formFinalizaVenda" servletRelativeAction="/venda/finalizar-venda" method="POST">
			<div class="row">
				<div class="col-sm-12" align="right">
					<c:if test="${not empty carrinho }">
						<a class="btn btn-primary"
							data-href="<c:url value="/venda/cancelar-venda"/>"
							data-toggle="modal" data-target="#confirm-delete"> Cancelar
							Venda </a>
						<button name="submit" type="submit" class="btn btn-success">Finalizar Venda</button>
							<b>Desconto:</b> <input id="desconto" name="desconto" class="only-num"
								size="3" value="${desconto }" autofocus="autofocus" /> <b>%</b>
					</c:if>
				</div>
			</div>
		</form:form>
		
		<br>
		<br>
		
		<c:if test="${empty carrinho }">
			<div class="alert alert-warning" role="alert">Nenhum produto adicionado ao carrinho.</div>
		</c:if>

		<c:if test="${not empty carrinho }">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Código do Produto</th>
						<th>Nome</th>
						<th>Quantidade</th>
						<th>Valor R$</th>
						<th>Retirar</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${carrinho }" var="produto" >
						<tr>
							<td>${produto.key.codProduto }</td>
							<td>${produto.key.nome }</td>
							<td>${produto.value }</td>
							<td>R$ ${produto.key.valorUnitSaida * produto.value}</td>
							<td><a class="btn btn-primary"
									href="<c:url value="/venda/remover-do-carrinho/${produto.key.id}"/>"> 
								<span class="glyphicon glyphicon-trash"></span>
							</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
	
	<div id="confirm-delete" class="modal" role="dialog" tabindex="-1"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Cancelar Venda</h4>
					</div>
					<div class="modal-body">
						<p>Você está a ponto de cancelar uma venda, esse procedimento é irreversível.</p>
						<p>Você deseja continuar?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-primary btn-ok">Confirmar</a>
					</div>
				</div>
			</div>
		</div>

	<jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>
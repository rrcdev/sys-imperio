<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
</head>
<body>
	<div class="bs-component">
		<nav class="navbar navbar-static navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
						<span class="sr-only">SysImpério</span> 
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand"
						href="#"><img
						src="<c:url value="/resources/images/logo-imperio.jpg" />"
						alt="SysImpério" 
						class="logo-imperio">
					</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav">
						<li><a
							href="<c:url value="/produto/listar"></c:url>">Produtos<span
								class="sr-only">(current)</span></a></li>
						<li><a
							href="<c:url value="/venda/listar"></c:url>">Vendas<span
								class="sr-only">(current)</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</body>
</html>
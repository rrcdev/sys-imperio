<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- <div class="footer"> -->
<!-- 	<div class="container"> -->
<!-- 		<p class="text-muted text-rrc">RRC Software Development - Todos os direitos reservados &copy;</p> -->
<!-- 	</div> -->
<!-- </div> -->

<script src="<c:url value="/webjars/jquery/2.1.4/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/3.3.5/js/bootstrap.min.js" />"></script>

<script src="<c:url value="/webjars/jquery-validation/1.12.0/jquery.validate.min.js" />"></script>
<script src="<c:url value="/resources/js/funcoes.js" />"></script>

<script	src="<c:url value="/webjars/jquery-ui/1.11.4/jquery-ui.min.js" />"></script>

<script src="<c:url value="/webjars/jquery-validation/1.12.0/jquery.validate.min.js" />"></script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="<c:url value="/webjars/bootstrap/3.3.5/css/bootstrap.min.css" />" rel="stylesheet" />
<link href="<c:url value="/resources/css/bootstrap-simplex.min.css" />" rel="stylesheet" />
<link href="<c:url value="/resources/css/imperio.css" />" rel="stylesheet" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
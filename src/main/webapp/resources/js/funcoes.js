$(document).ready(function() {
	
	$('a.back').click(function() {
		parent.history.back();
		return false;
	});
	
	$('.valid-cod').keyup(function() {
		$(this).val($(this).val().toUpperCase());
	});
	
	$('.valid-num').keyup(function() {
		$(this).val($(this).val().replace(" ",''));
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
	});
	
	$('.only-num').keyup(function() {
		$(this).val($(this).val().replace(" ",''));
		$(this).val($(this).val().replace(/[^0-9]/g,''));
	});
	
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
	
	$('#cadastrarProduto').validate({
		rules : {
			codProduto : {
				required : true
			},
			nome : {
				required : true
			},
			qtdEstoque : {
				required : true
			},
			valorUnitEntrada : {
				required : true
			},
			valorUnitSaida : {
				required : true
			},
		},
		highlight : function(element) {
			$(element).closest('.form-group')
					.addClass('has-error');
		},
		unhighlight : function(element) {
			$(element).closest('.form-group')
					.removeClass('has-error');
		},
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error,
				element) {
			error.insertAfter(element.parent()
					.children().last());
			var itemForm = element.parent();
			var id = element.attr("name");
			$(itemForm).find("span").attr("id",
					id);
		},
		messages : {
			codProduto : {
				required : "Código do produto obrigatório."
			},
			nome : {
				required : "Nome do produto obrigatório."
			},
			qtdEstoque : {
				required : "Quantidade de estoque não pode ser vazia."
			},
			valorUnitEntrada : {
				required : "Valor unitário de entrada não pode ser vazio."
			},
			valorUnitSaida : {
				required : "Valor unitário de saída não pode ser vazio."
			},
		}
	});
	
	$('div.error-validation:has(span)').find('span').css(
			'color', '#a94442');
	$('div.error-validation:has(span)').find('span').parent()
			.parent().parent().addClass(
					'has-error has-feedback');
	
});
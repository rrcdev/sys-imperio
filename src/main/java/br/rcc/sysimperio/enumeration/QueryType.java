package br.rcc.sysimperio.enumeration;

public enum QueryType {
	
	JPQL, NATIVE, NAMED
}

package br.rcc.sysimperio.repository.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

import br.rcc.sysimperio.enumeration.QueryType;
import br.rcc.sysimperio.repository.GenericRepository;

@Named
public class JpaGenericRepositoryImpl<T> implements GenericRepository<T> {

	protected EntityManager em;
	
	@Override
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional
	public void save(T entity) {
		this.em.persist(entity);
	}

	@Override
	@Transactional
	public void update(T entity) {
		this.em.merge(entity);
	}

	@Override
	@Transactional
	public void delete(T entity) {
		this.em.remove(em.merge(entity));
	}

	@Override
	@Transactional(readOnly = true)
	public T find(Class<T> entityClass, Object id) {
		T result = null;
		result = em.find(entityClass, id);
		return result;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<T> find(Class<T> entityClass) {
		CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(entityClass);
		cq.from(entityClass);
		return em.createQuery(cq).getResultList();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	@Transactional(readOnly = true)
	public List find(String queryName, Map<String, Object> namedParams) {

		return find(QueryType.NAMED, queryName, namedParams);
	}

	@Override
	@SuppressWarnings("rawtypes")
	@Transactional(readOnly = true)
	public List find(QueryType type, String query, Map<String, Object> namedParams) {
		
		return find(type, query, namedParams, -1, -1);
	}

	@Override
	@SuppressWarnings("rawtypes")
	@Transactional(readOnly = true)
	public List find(String queryName, Map<String, Object> namedParams, int firstResult, int maxResults) {
		
		return find(QueryType.NAMED, queryName, namedParams, firstResult, maxResults);
	}

	@Override
	@SuppressWarnings("rawtypes")
	@Transactional(readOnly = true)
	public List find(QueryType type, String query, Map<String, Object> namedParams, int firstResult, int maxResults) {
		List result = null;
		Query q;
		
		if (type == QueryType.JPQL) {
			q = em.createQuery(query);
		} else if (type == QueryType.NATIVE) {
			q = em.createNativeQuery(query);
		} else if (type == QueryType.NAMED) {
			q = em.createNamedQuery(query);	
		} else {
			throw new IllegalArgumentException("Invalid query type: " + type);
		}
		
		setNamedParameters(q, namedParams);
		
		if (firstResult >= 0 && maxResults >= 0) {
			q = q.setFirstResult(firstResult).setMaxResults(maxResults);
		}
		
		result = q.getResultList();
		
		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public Object findFirst(String queryName, Map<String, Object> namedParams) {

		return findFirst(QueryType.NAMED, queryName, namedParams);
	}

	@Override
	@Transactional(readOnly = true)
	public Object findFirst(QueryType type, String query, Map<String, Object> namedParams) {
		
		@SuppressWarnings("rawtypes")
		List result = find(type, query, namedParams, 0, 1);
		return result == null || result.size() == 0 ? null : result.get(0);
	}
	
	private void setNamedParameters(Query q, Map<String, Object> namedParams) {
		if (namedParams != null) {
			Set<String> keys = namedParams.keySet();
			for (String key : keys) {
				q.setParameter(key, namedParams.get(key));
			}
		}
	}

}

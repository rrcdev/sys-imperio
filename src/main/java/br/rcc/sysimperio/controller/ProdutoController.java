package br.rcc.sysimperio.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.rcc.sysimperio.model.Produto;
import br.rcc.sysimperio.service.ProdutoService;

@Controller
@RequestMapping("produto")
public class ProdutoController {

	@Inject
	private ProdutoService produtoService;

	@RequestMapping(value = { "/", "" })
	public String index() {

		return "redirect:/produto/listar";
	}

	@RequestMapping(value = { "listar" }, method = RequestMethod.GET)
	public String listar(Model model) {

		model.addAttribute("produtos", produtoService.find(Produto.class));

		return "produto/listar";
	}
	
	@RequestMapping(value = { "buscar" }, method = RequestMethod.POST)
	public String buscar(@RequestParam("codProduto") String codProduto, Model model, RedirectAttributes redirect) {
		
		List<Produto> produtos = produtoService.getProdutoByCodigo(codProduto);
		
		if (produtos != null && !produtos.isEmpty()) {
			model.addAttribute("produtos", produtos);
		} else {
			
			redirect.addFlashAttribute("erro", "Produto(s) não encontrado(s).");
			return "redirect:/produto/listar";
		}
		
		return "produto/listar";
	}

	@RequestMapping(value = { "cadastrar" }, method = RequestMethod.GET)
	public String cadastrar(Model model) {

		model.addAttribute("action", "cadastrar");
		model.addAttribute("produto", new Produto());

		return "produto/cadastrar";
	}

	@RequestMapping(value = { "cadastrar" }, method = RequestMethod.POST)
	public String cadastrar(Model model, @Valid @ModelAttribute("produto") Produto produto, BindingResult result,
			RedirectAttributes redirectAttributes) {

		model.addAttribute("action", "cadastrar");

		if (result.hasErrors()) {
			model.addAttribute("produto", produto);
			return "produto/cadastrar";
		}

		produtoService.save(produto);

		redirectAttributes.addFlashAttribute("info", "Produto cadastrado com sucesso.");
		return "redirect:/produto/listar";
	}

	@RequestMapping(value = { "editar/{id}" }, method = RequestMethod.GET)
	public String editar(@PathVariable("id") Long id, Model model) {

		Produto produto = produtoService.find(Produto.class, id);

		model.addAttribute("action", "editar");
		model.addAttribute("produto", produto);

		return "produto/cadastrar";
	}

	@RequestMapping(value = { "editar/{id}" }, method = RequestMethod.POST)
	public String editar(@PathVariable("id") Long id, @Valid @ModelAttribute("produto") Produto produto, Model model,
			BindingResult result, RedirectAttributes redirectAttributes) {

		model.addAttribute("action", "editar");

		if (result.hasErrors()) {
			model.addAttribute("produto", produto);
			return "produto/cadastrar";
		}

		produtoService.update(produto);
		redirectAttributes.addFlashAttribute("info", "Produto atualizado com sucesso.");
		return "redirect:/produto/listar";
	}

	@RequestMapping(value = { "remover/{id}" }, method = RequestMethod.GET)
	public String excluir(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Produto produto = produtoService.find(Produto.class, id);

		if (produto != null && produto.getQtdEstoque() >= 1) {
			produto.setQtdEstoque(produto.getQtdEstoque() - 1);
			produtoService.update(produto);
			redirectAttributes.addFlashAttribute("info", "Produto removido com sucesso.");
			
		} else if(produto.getQtdEstoque() == 0) {
			redirectAttributes.addFlashAttribute("erro", "Produto não se encontra mais em estoque. Por favor, renove o estoque deste produto.");
			
		} else {
			redirectAttributes.addFlashAttribute("erro", "Produto inexistente.");
			
		}
		return "redirect:/produto/listar";
	}
	
	@RequestMapping(value = {"detalhe/{id}"}, method = RequestMethod.GET)
	public String detalhes(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
		
		Produto produto = produtoService.find(Produto.class, id);
		
		if (produto != null) {
			model.addAttribute("produtoDetalhe", produto);
		} else {
			redirectAttributes.addFlashAttribute("erro", "Produto inexistente.");
		}
		
		return "produto/detalhe";
	}
}
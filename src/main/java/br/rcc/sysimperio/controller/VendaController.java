package br.rcc.sysimperio.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.rcc.sysimperio.model.Produto;
import br.rcc.sysimperio.model.ProdutoVenda;
import br.rcc.sysimperio.model.Venda;
import br.rcc.sysimperio.service.ProdutoService;
import br.rcc.sysimperio.service.VendaService;

@Controller
@RequestMapping("venda")
public class VendaController {

	@Inject
	private VendaService vendaService;

	@Inject
	private ProdutoService produtoService;
	
	private static Map<Produto, Integer> carrinho = new HashMap<Produto, Integer>();
	private static Double valorTotal = 0.0;
	
	@RequestMapping(value = { "/", "" })
	public String index() {

		return "redirect:/venda/listar";
	}

	@RequestMapping(value = { "listar" }, method = RequestMethod.GET)
	public String listar(Model model) {

		model.addAttribute("vendas", vendaService.find(Venda.class));

		return "venda/listar";
	}

	@RequestMapping(value = { "buscar" }, method = RequestMethod.GET)
	public String buscar(Model model) {
		
		model.addAttribute("action", "buscar");
		model.addAttribute("carrinho", carrinho);
		model.addAttribute("valorTotal", valorTotal);
		
		return "venda/buscar";
	}

	@RequestMapping(value = { "buscar" }, method = RequestMethod.POST)
	public String buscar(@RequestParam("codProduto") String codProduto, Model model,
			RedirectAttributes redirectAttributes) {

		model.addAttribute("codProduto", codProduto);
		model.addAttribute("carrinho", carrinho);
		model.addAttribute("valorTotal", valorTotal);

		List<Produto> produtos = produtoService.getProdutoByCodigo(codProduto);

		if (produtos != null && !produtos.isEmpty()) {
			model.addAttribute("produtos", produtos);
		} else {
			redirectAttributes.addFlashAttribute("erro", "Produto não encontrado.");
			return "redirect:/venda/buscar";
		}

		return "venda/buscar";
	}
	
	@RequestMapping(value = { "adicionar-no-carrinho/{id}" }, method = RequestMethod.POST)
	public String adicionarProduto(@RequestParam(value = "quantidade") Integer quantidade, 
			@PathVariable("id") Long id,  
			Model model, RedirectAttributes redirect) {

		model.addAttribute("quantidade", quantidade);
		
		Produto produto = produtoService.find(Produto.class, id);
		
		if (quantidade == 0) {
			
			redirect.addFlashAttribute("erro", "Não se pode adicionar no carrinho um produto com quantidade 0.");
			
		} else if (quantidade <= produto.getQtdEstoque()) {
			
			if (carrinho.containsKey(produto) && 
					(carrinho.get(produto) >= produto.getQtdEstoque())) {
				
				redirect.addFlashAttribute("erro", "Não é mais possível adicionar o produto " +produto.getCodProduto() + " na venda, pois já atingiu seu limite de estoque.");
				return "redirect:/venda/buscar";
				
			} else if (carrinho.containsKey(produto)) {
				Integer tmpQuantidade; 
				tmpQuantidade = carrinho.get(produto) + quantidade;
				
				carrinho.put(produto, tmpQuantidade);
				
			} else {
				
				carrinho.put(produto, quantidade);
			}
			
			valorTotal += produto.getValorUnitSaida() * quantidade;
			
		} else {
			redirect.addFlashAttribute("erro", "Quantidade deve ser igual ou maior que a quantidade do produto em estoque.");
		}
		
		model.addAttribute("valorTotal", valorTotal);
		return "redirect:/venda/buscar";
	}

	@RequestMapping(value = { "remover-do-carrinho/{id}" }, method = RequestMethod.GET)
	public String removeProduto(@PathVariable("id") Long id, Model model) {

		Produto produto = produtoService.find(Produto.class, id);
		valorTotal -= produto.getValorUnitSaida() * carrinho.get(produto);
		carrinho.remove(produto);

		return "redirect:/venda/buscar";
	}

	@RequestMapping(value = { "finalizar-venda" }, method = RequestMethod.POST)
	public String finalizarVenda(@RequestParam("desconto") Double desconto, Model model, RedirectAttributes redirect) {
		
		if (!carrinho.isEmpty()) {
			
			Venda venda = new Venda();
			venda.setCodigoVenda(getCodigoVenda());
			venda.setDataVenda(new DateTime().toDate());
			
			ProdutoVenda pv = null;
			
			for (Map.Entry<Produto, Integer> entry : carrinho.entrySet()) {
				pv = new ProdutoVenda();
				
				entry.getKey().setQtdEstoque(entry.getKey().getQtdEstoque() - entry.getValue());
				
				produtoService.update(entry.getKey());
				
				pv.setProduto(entry.getKey());
				pv.setQuantidade(entry.getValue());
				pv.setVenda(venda);
				
				venda.getProdutosVenda().add(pv);
				
			}
			
			if (desconto != null && desconto != 0.0) {
				valorTotal = calculaDesconto(valorTotal, desconto);
			}
			
			venda.setValorTotal(valorTotal);
			
			vendaService.save(venda);
			
			carrinho.clear();
			valorTotal = 0.0;
			
			redirect.addFlashAttribute("info", "Venda realizada com sucesso.");
			
		} else {
			redirect.addAttribute("erro", "Não há produtos no carrinho, não é possível finalizar a venda.");
		}
		
		return "redirect:/venda/listar";
	}
	
	@RequestMapping(value = { "cancelar-venda" }, method = RequestMethod.GET)
	public String cancelarVenda(RedirectAttributes redirect) {
		
		if (!carrinho.isEmpty()) {
			carrinho.clear();
			valorTotal = 0.0;
			
			redirect.addFlashAttribute("info", "Venda cancelada com sucesso.");
			
		}
		
		return "redirect:/venda/buscar";
	}

	@RequestMapping(value = "detalhes/{id}", method = RequestMethod.GET)
	public String detalhes(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {

		Venda venda = vendaService.getVendaWithProdutos(id);

		if (venda == null) {
			
			redirectAttributes.addFlashAttribute("erro", "Venda inexistente");
			return "redirect:/venda/listar";
			
		} else {
			
			model.addAttribute("venda", venda);
			return "venda/detalhe";
			
		}
	}
	
	private String getCodigoVenda() {
		DateTime dt = new DateTime();
		return "VENDA-" + dt.getDayOfMonth()+ "-" + dt.getMonthOfYear() + "-" + dt.getYear() + "-" + dt.getHourOfDay() + "-" + dt.getMinuteOfHour();
	}
	
	private Double calculaDesconto(Double valorTotal, Double desconto) {
		
		Double valorDescontado = 0.0;
		valorDescontado = valorTotal - ((valorTotal * desconto) / 100);
		
		return valorDescontado;
	}

}

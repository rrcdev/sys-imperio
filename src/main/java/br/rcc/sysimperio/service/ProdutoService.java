package br.rcc.sysimperio.service;

import java.util.List;

import br.rcc.sysimperio.model.Produto;

public interface ProdutoService extends GenericService<Produto>{

	public List<Produto> getProdutoByCodigo(String codProduto);
	
}

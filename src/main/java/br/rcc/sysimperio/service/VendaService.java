package br.rcc.sysimperio.service;

import br.rcc.sysimperio.model.Venda;

public interface VendaService extends GenericService<Venda>{
	
	public Venda getVendaWithProdutos(Long id);

}

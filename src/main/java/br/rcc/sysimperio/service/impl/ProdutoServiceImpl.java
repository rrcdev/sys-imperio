package br.rcc.sysimperio.service.impl;

import java.util.List;

import javax.inject.Named;

import br.rcc.sysimperio.model.Produto;
import br.rcc.sysimperio.service.ProdutoService;
import br.rcc.sysimperio.util.SimpleMap;

@Named
public class ProdutoServiceImpl extends GenericServiceImpl<Produto> implements ProdutoService{

	@SuppressWarnings("unchecked")
	@Override
	public List<Produto> getProdutoByCodigo(String codProduto) {
		return (List<Produto>) find("Produto.findProdutoByCodigo", new SimpleMap<String, Object>("codProduto", "%"+codProduto+"%"));
	}

}

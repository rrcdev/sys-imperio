package br.rcc.sysimperio.service.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import br.rcc.sysimperio.enumeration.QueryType;
import br.rcc.sysimperio.repository.GenericRepository;
import br.rcc.sysimperio.service.GenericService;

@Named
public class GenericServiceImpl<T> implements GenericService<T> {

	@Inject
	private GenericRepository<T> repository;
	
	@Transactional
	@Override
	public void save(T entity) {
		repository.save(entity);
	}

	@Transactional
	@Override
	public void update(T entity) {
		repository.update(entity);
	}

	@Override
	public void delete(T entity) {
		repository.delete(entity);
	}
	
	@Transactional(readOnly = true)
	@Override
	public T find(Class<T> entityClass, Object id) {
		return repository.find(entityClass, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> find(Class<T> entityClass) {
		return repository.find(entityClass);
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("rawtypes")
	public List find(String queryName, Map<String, Object> namedParams) {
		return find(queryName, namedParams, -1, -1);
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("rawtypes")
	public List find(QueryType type, String query, Map<String, Object> namedParams) {
		return find(type, query, namedParams, -1, -1);
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("rawtypes")
	public List find(String queryName, Map<String, Object> namedParams, int firstResult, int maxResults) {
		return find(QueryType.NAMED, queryName, namedParams, firstResult, maxResults);
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("rawtypes")
	public List find(QueryType type, String query, Map<String, Object> namedParams, int firstResult, int maxResults) {
		return repository.find(type, query, namedParams, firstResult, maxResults);
	}

	@Override
	@Transactional(readOnly = true)
	public Object findFirst(String queryName, Map<String, Object> namedParams) {
		return findFirst(QueryType.NAMED, queryName, namedParams);
	}

	@Override
	@Transactional(readOnly = true)
	public Object findFirst(QueryType type, String query, Map<String, Object> namedParams) {
		return repository.findFirst(type, query, namedParams);
	}

}
package br.rcc.sysimperio.service.impl;

import javax.inject.Named;

import br.rcc.sysimperio.model.Venda;
import br.rcc.sysimperio.service.VendaService;
import br.rcc.sysimperio.util.SimpleMap;

@Named
public class VendaServiceImpl extends GenericServiceImpl<Venda> implements VendaService{
	
	@Override
	public Venda getVendaWithProdutos(Long id) {
		return (Venda) findFirst("Venda.findVendaWithProdutos", new SimpleMap<String, Object>("id", id));
	}

}

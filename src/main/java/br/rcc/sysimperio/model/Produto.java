package br.rcc.sysimperio.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQueries({
	@NamedQuery(name="Produto.findProdutoByCodigo", query = "select p from Produto p where p.codProduto LIKE :codProduto")
})
public class Produto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PRODUTO_ID")
	private Long id;
	
	@NotNull(message = "Quantidade de estoque não pode ser vazia.")
	@Min(value = 0, message = "O valor mínimo é 0.")
	private Integer qtdEstoque;
	
	@NotNull(message = "Valor unitário de entrada não pode ser vazio.")
	@Min(value = 0, message = "Valor unitário de entrada não pode ser menor que 0.")
	private Double valorUnitEntrada;
	
	@NotNull(message = "Valor unitário de saída não pode ser vazio.")
	private Double valorUnitSaida;
	
	@NotEmpty(message = "Nome do produto não pode ser vazio.")
	private String nome;
	
	private String descricao;
	
	@NotEmpty(message = "Código do produto não pode ser vazio.")
	private String codProduto;
	
	@OneToMany(mappedBy = "pk.produto", cascade = CascadeType.PERSIST)
	private Set<ProdutoVenda> produtosVenda;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodProduto() {
		return codProduto;
	}
	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getQtdEstoque() {
		return qtdEstoque;
	}
	public void setQtdEstoque(Integer qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}
	public Double getValorUnitEntrada() {
		return valorUnitEntrada;
	}
	public void setValorUnitEntrada(Double valorUnitEntrada) {
		this.valorUnitEntrada = valorUnitEntrada;
	}
	public Double getValorUnitSaida() {
		return valorUnitSaida;
	}
	public void setValorUnitSaida(Double valorUnitSaida) {
		this.valorUnitSaida = valorUnitSaida;
	}
	
	@Override
	public String toString() {
		return "Produto [id=" + id + ", codProduto=" + codProduto + ", nome=" + nome + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codProduto == null) ? 0 : codProduto.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codProduto == null) {
			if (other.codProduto != null)
				return false;
		} else if (!codProduto.equals(other.codProduto))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}

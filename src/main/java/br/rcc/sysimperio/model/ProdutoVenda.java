package br.rcc.sysimperio.model;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

@Entity
@AssociationOverrides({
		@AssociationOverride(name = "pk.produto", joinColumns = @JoinColumn(name = "PRODUTO_ID")),
		@AssociationOverride(name = "pk.venda", joinColumns = @JoinColumn(name = "VENDA_ID"))
})
public class ProdutoVenda implements Serializable{

	private static final long serialVersionUID = 1L;
	private ProdutoVendaId pk = new ProdutoVendaId();
	private Integer quantidade;
	
	@EmbeddedId
	public ProdutoVendaId getPk() {
		return pk;
	}
	public void setPk(ProdutoVendaId pk) {
		this.pk = pk;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	@Transient
	public Produto getProduto(){
		return getPk().getProduto();
	}
	public void setProduto(Produto produto){
		getPk().setProduto(produto);
	}
	@Transient
	public Venda getVenda(){
		return getPk().getVenda();
	}
	public void setVenda(Venda venda){
		getPk().setVenda(venda);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoVenda other = (ProdutoVenda) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		if (quantidade == null) {
			if (other.quantidade != null)
				return false;
		} else if (!quantidade.equals(other.quantidade))
			return false;
		return true;
	}
	
	
}
